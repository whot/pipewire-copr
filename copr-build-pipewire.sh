#!/bin/bash -xe
#
# wget and execute this script with the project name as argument, it will do the rest

project="$1"
[[ -n "$project" ]] || (echo "Usage: $(basename $0) <pipwire|wireplumber|media-session>" && exit 1)
echo "Building $project"

# COPR expects the spec and tarball in there
results_dir="results"

# upstream pipewire configuration
repo="https://gitlab.freedesktop.org/pipewire/$project"
branch="master"
api_endpoint="https://gitlab.freedesktop.org/api/v4/projects/pipewire%2F$project"
meson_build_url="$repo/raw/$branch/meson.build"
last_commit_url="$api_endpoint/repository/commits/$branch"

# The special repo where I keep the specfile maker script
copr_src_repo="https://gitlab.freedesktop.org/whot/pipewire-copr"
raw_path="$copr_src_repo/raw/master"
specfile="$raw_path/$project.spec.in"

mkdir -p "$results_dir"

date=$(date +%Y%m%d%H%M)
# Extract the current version from meson.build
version=$(curl $meson_build_url | \
          grep -A 3 'project(' |grep '\<version\> :' | \
          sed -e "s|.*version : '\(.*\)',|\1|")

major=$(echo $version | cut -f1 -d'.')
minor=$(echo $version | cut -f2 -d'.')
patchlevel=$(echo $version | cut -f3 -d'.')

# Extract the current sha from the API
sha=$(curl "$api_endpoint/repository/commits/$branch" | \
    sed -e 's|{"id":"\([[:alnum:]]\{40\}\).*|\1|')
shortcommit=${sha:0:7}

if [[ -e "$(basename $specfile)" ]]; then
    specfile=file://$PWD/$(basename $specfile)
fi

tarball="$repo/-/archive/$sha/$project-$sha.tar.gz"
# Put the specfile and the tarball into the results/ directory. COPR will
# pick it up from there and build the rpms.
curl "$specfile" | \
    sed -e "s|@GITDATE@|$date|" \
        -e "s|@GITVERSION@|$sha|" \
        -e "s|@MAJOR@|$major|" \
        -e "s|@MINOR@|$minor|" \
        -e "s|@PATCHLEVEL@|$patchlevel|" \
    > "$results_dir/$project.spec"

grep "^Patch" "$results_dir/$project.spec" | sed -e "s|^Patch\w*:[ \t]\+\(.*\.patch\)|\1|" > patches.list

for patch in $(cat patches.list); do
    curl --output $results_dir/$patch $raw_path/$patch
done
curl -o "$results_dir/$project-$shortcommit.tar.gz" "$tarball"
